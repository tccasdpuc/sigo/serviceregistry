#
# Build stage
#
FROM maven:3.6.0-jdk-8-slim AS build
COPY src /home/serviceregistry/src
COPY pom.xml /home/serviceregistry
RUN mvn -f /home/serviceregistry/pom.xml clean package -Dmaven.test.skip=true

#
# Package stage
#
FROM openjdk:8-jre-slim
COPY --from=build /home/serviceregistry/target /usr/local/lib/serviceregistry
EXPOSE 8761
ENTRYPOINT ["java","-jar","/usr/local/lib/serviceregistry/serviceregistry-0.1.jar"]